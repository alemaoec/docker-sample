A simple example of docker

# Building and running the application

```
git clone git@gitlab.com:alemaoec/docker-sample.git
cd docker-sample/
docker build -t sampleapp .
docker run -p 4000:80 sampleapp
Open localhost:4000 
```